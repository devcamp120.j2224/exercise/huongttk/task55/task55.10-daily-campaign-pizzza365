$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // bạn có thê dùng để lưu trữ combo được chọn, mỗi khi khách chọn bạn lại đổi giá trị properties của nó
  var gSelectedMenuStructure = {
    menuName: "",    // S, M, L
    duongKinhCM: 0,
    suonNuong: 0,
    salad: 0,
    drink: 0,
    priceVND: 0
  };
  var gSelectedMenuStructure = getComboOrder("", 0, 0, "", 0, 0);   //đầu tiên gán cho nó giá trị mặc định
  // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
  var gSelectedPizzaType = "";
  // dùng để lưu thông tin khách đặt hàng
  var gUser = {
    menuDuocChon: "",
    loaiPizza: "",
    loaiNuocUong: "",
    hoVaTen: "",
    email: "",
    dienThoai: "",
    diaChi: "",
    loiNhan: "",
    voucher: "",
    phanTramGiamGia: ""
  };
  var gDiscount = "";
  var gId = "";
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  // Hàm gán sự kiện khi ấn nút chọn comno Small
  $("#btn-combo-small").on("click", onBtnComboSmall)
  // Hàm gán sự kiện khi ấn nút chọn comno Medium
  $("#btn-combo-medium").on("click", onBtnComboMedium)
  // Hàm gán sự kiện khi ấn nút chọn comno Large
  $("#btn-combo-large").on("click", onBtnComboLarge)
  // Hàm gán sự kiện khi ấn nút chọn Pizza Hawai
  $("#btn-pizza-hawai").on("click", onBtnHawaiPizza);
  // Hàm gán sự kiện khi ấn nút chọn Pizza Seafood
  $("#btn-pizza-seafood").on("click", onBtnSeafoodPizza);
  // Hàm gán sự kiện khi ấn nút chọn Pizza Bacon
  $("#btn-pizza-bacon").on("click", onBtnBaconPizza);
  // gán sự kiện khi ấn nút gửi đơn
  $("#btn-gui-don").on("click", onBtnGuiDon);
  // gán sự kiện khi ấn nút Tạo Đơn
  $("#btn-tao-don").on("click", onBtnTaoDonClick)
  // gán sự kiện khi ấn nút quay lại
  $("#btn-quay-lai").on("click", onBtnQuayLaiClick)

  //  Gán sự kiện tải trang
  onPageLoading();
   //
   getMessage();


  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // Hàm xử lý sự kiện khi ấn nút chọn Combo Small
  function onBtnComboSmall() {
    //tạo một đối tượng combo, được tham số hóa
    var vMenuSmall = getComboOrder("Combo Small", "20", "2", "200g", "2", 150000);
    // đổi màu button
    changeComboButtonsColor(vMenuSmall);
    // gán giá trị của combo S được chọn vào biến toàn cục để lưu tại đó
    gSelectedMenuStructure = vMenuSmall;
    // ghi ra console
    console.log(gSelectedMenuStructure);
  };

  // Hàm xử lý sự kiện khi ấn nút chọn Combo Medium
  function onBtnComboMedium() {
    //tạo một đối tượng combo, được tham số hóa
    var vMenuMedium = getComboOrder("Combo Medium", "25", "4", "300g", "3", 200000);
    // đổi màu button
    changeComboButtonsColor(vMenuMedium);
    // gán giá trị của combo M được chọn vào biến toàn cục để lưu tại đó
    gSelectedMenuStructure = vMenuMedium;
    // ghi ra console
    console.log(gSelectedMenuStructure);
  };

  // Hàm xử lý sự kiện khi ấn nút chọn Combo Large
  function onBtnComboLarge() {
    //tạo một đối tượng combo, được tham số hóa
    var vMenuLarge = getComboOrder("Combo Large", "30", "8", "500g", "4", 250000);
    // đổi màu button
    changeComboButtonsColor(vMenuLarge);
    // gán giá trị của combo L được chọn vào biến toàn cục để lưu tại đó
    gSelectedMenuStructure = vMenuLarge;
    // ghi ra console
    console.log(gSelectedMenuStructure);
  };

  // hàm xử lý sự kiện khi ấn nút chọn pizza Hawai
  function onBtnHawaiPizza() {
    "use strict";
    //tạo một đối tượng combo, được tham số hóa
    var vPizzaHawai = "Pizza Hawai";
    // đổi màu button
    changPizzaTypeButtonsColor(vPizzaHawai);
    // gán giá trị của pizza Hawai được chọn vào biến toàn cục để lưu tại đó
    gSelectedPizzaType = vPizzaHawai;
    // ghi ra console
    console.log(gSelectedPizzaType);
  };

  // hàm xử lý sự kiện khi ấn nút chọn pizza Seafood
  function onBtnSeafoodPizza() {
    "use strict";
    //tạo một đối tượng combo, được tham số hóa
    var vPizzaSeafood = "Pizza Seafood";
    // đổi màu button
    changPizzaTypeButtonsColor(vPizzaSeafood);
    // gán giá trị của pizza Seafood được chọn vào biến toàn cục để lưu tại đó
    gSelectedPizzaType = vPizzaSeafood;
    // ghi ra console
    console.log(gSelectedPizzaType);
  };

  // hàm xử lý sự kiện khi ấn nút chọn pizza Bacon
  function onBtnBaconPizza() {
    "use strict";
    //tạo một đối tượng combo, được tham số hóa
    var vPizzaBacon = "Pizza Bacon";
    // đổi màu button
    changPizzaTypeButtonsColor(vPizzaBacon);
    // gán giá trị của pizza Bacon được chọn vào biến toàn cục để lưu tại đó
    gSelectedPizzaType = vPizzaBacon;
    // ghi ra console
    console.log(gSelectedPizzaType);
  };

  // HÀM XỬ LÝ SỰ KIỆN KHI LOAD TRANG
  function onPageLoading() {
    // load dữ liệu vào ô select đồ uống
    loadDataDrinkToSelect();
    getMessage();
   
  };
   /// task55.10
   function getMessage() {
    $.ajax({
      type: "GET",
      url: "http://localhost:8080/message",
      success: function (response) {
        console.log(response);
        showMessage(response);
      },
      error: function (err) {
        console.log(err);
      },
    });
  }

  // HÀM XỬ LÝ SỰ KIỆN KHI ẤN NÚT GỬI ĐƠN
  function onBtnGuiDon() {
    "use strict";
    // B1: thu thập dữ liệu 
    getDaTaUser(gUser);
    //console.log(gUser);
    // B2: kiểm tra dữ liêu
    var vCheck = validateDataUser(gUser);
    console.log(gUser);
    if (vCheck) {
      // B3: hiển thị dữ liệu lên modal
      showDataUser(gUser)
      console.log(gUser);
      $("#user-modal").modal("show");
    }
  };

  // HÀM XỬ LÝ SỰ KIỆN KHI ẤN NÚT TẠO ĐƠN TRÊN MODAL
  function onBtnTaoDonClick() {
    "use strict";
    // ẩn modal gửi đơn hàng
    $("#user-modal").modal("hide")

    // gọi sever để thêm đơn hàng mới
    createOrder();
  };
   
  // HÀM XỬ LÝ SỰ KIỆN KHI ẤN NÚT TẠO ĐƠN
  function onBtnQuayLaiClick(){
    "use strict";
     // ẩn modal gửi đơn hàng
     $("#user-modal").modal("hide")
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // thu thập dữ liệu vào biến toàn cục gSelectedMenuStructure
  function getComboOrder(paramMenuName, paramDuongKinh, paramSuonNuong, paramSalad, paramDrink, paramPriceVND) {
    var vCombo = {
      menuName: paramMenuName,
      duongKinhCM: paramDuongKinh,
      suonNuong: paramSuonNuong,
      salad: paramSalad,
      drink: paramDrink,
      priceVND: paramPriceVND
    }
    return vCombo;
  };
  // Hàm đổi màu nút chọn khi chọn Combo Menu
  function changeComboButtonsColor(paramCombo) {
    if (paramCombo.menuName == "Combo Small") {
      $("#btn-combo-small").removeClass("btn-warning")
      $("#btn-combo-small").addClass("btn-success");
      $("#btn-combo-medium").addClass(" btn-warning");
      $("#btn-combo-large").addClass("btn-warning");

    };
    if (paramCombo.menuName == "Combo Medium") {
      $("#btn-combo-small").addClass(" btn-warning");
      $("#btn-combo-medium").removeClass("btn-warning")
      $("#btn-combo-medium").addClass("btn-success");
      $("#btn-combo-large").addClass("btn-warning");
    }
    if (paramCombo.menuName == "Combo Large") {
      $("#btn-combo-small").addClass("btn-warning");
      $("#btn-combo-medium").addClass(" btn-warning");
      $("#btn-combo-large").removeClass("btn-warning")
      $("#btn-combo-large").addClass("btn-success");
    }
  };
  // Hàm đổi màu nút chọn khi chọn Pizza Type
  function changPizzaTypeButtonsColor(paramCombo) {
    if (paramCombo == "Pizza Hawai") {
      $("#btn-pizza-hawai").removeClass("btn-warning")
      $("#btn-pizza-hawai").addClass("btn-success");
      $("#btn-pizza-seafood").addClass(" btn-warning");
      $("#btn-pizza-bacon").addClass("btn-warning");

    };
    if (paramCombo == "Pizza Seafood") {
      $("#btn-pizza-hawai").addClass(" btn-warning");
      $("#btn-pizza-seafood").removeClass("btn-warning")
      $("#btn-pizza-seafood").addClass("btn-success");
      $("#btn-pizza-bacon").addClass("btn-warning");
    }
    if (paramCombo == "Pizza Bacon") {
      $("#btn-pizza-hawai").addClass("btn-warning");
      $("#btn-pizza-seafood").addClass(" btn-warning");
      $("#btn-pizza-bacon").removeClass("btn-warning")
      $("#btn-pizza-bacon").addClass("btn-success");
    }
  };

  // HÀM LOAD DỮ LIỆU VÀO Ô SELECT ĐỒ UỐNG
  function loadDataDrinkToSelect() {
    "use strict";
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
      type: "GET",
      dataType: "json",
      success: function (responseObject) {
        getDrinkFromObject(responseObject);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  };
  // hàm nhận Data Drink khi gọi Ajax
  function getDrinkFromObject(paramDrinkObj) {
    "use strict";
    console.log(paramDrinkObj)
    for (var i = 0; i < paramDrinkObj.length; i++) {
      $('#select-drink').append($('<option>', {
        value: paramDrinkObj[i].maNuocUong,
        text: paramDrinkObj[i].tenNuocUong,
      }));
    }
  };

  // HÀM THU THẬP DỮ LIỆU CỦA KHÁCH ĐẶT HÀNG
  function getDaTaUser(paramUser) {
    "use strict";
    paramUser.hoVaTen = $("#inp-fullname").val().trim();
    paramUser.email = $("#inp-email").val().trim();
    paramUser.dienThoai = $("#inp-dien-thoai").val().trim();
    paramUser.diaChi = $("#inp-dia-chi").val().trim();
    paramUser.loiNhan = $("#inp-message").val().trim();
    paramUser.voucher = $("#inp-voucher").val().trim();
    paramUser.menuDuocChon = gSelectedMenuStructure;
    paramUser.loaiPizza = gSelectedPizzaType;
    paramUser.phanTramGiamGia = gDiscount.phanTramGiamGia;
    paramUser.loaiNuocUong = $("#select-drink").val();
  };
  // HÀM KIỂM TRA DỮ LIỆU
  function validateDataUser(paramUser) {
    "usse strict";
    var vEmall = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (paramUser.menuDuocChon.menuName === "") {
      alert("Bạn chưa chọn kích cỡ Combo");
      return false;
    }
    if (paramUser.loaiPizza === "") {
      alert("Bạn chưa chọn loại Pizza yêu thích");
      return false;
    }
    if (paramUser.loaiNuocUong === "0") {
      alert("Bạn chưa chọn loại đồ uống");
      return false;
    }
    if (paramUser.hoVaTen === "") {
      alert("Bạn cần nhập đẩy đủ Họ và Tên");
      return false;
    }
    if (paramUser.email === "") {
      alert("Bạn chưa nhập Email");
      return false;
    }
    if (!vEmall.test(paramUser.email)) {
      alert("Email không hợp lệ")
      return false;
    }
    if (paramUser.dienThoai === "") {
      alert("Số điện thoại không được bỏ trống");
      return false;
    }
    if (isNaN(paramUser.dienThoai)) {
      alert("Số điện thoại phải là dạng số");
      return false;
    }
    if (paramUser.diaChi === "") {
      alert("Địa chỉ không được bỏ trống");
      return false;
    }
    return true;
  };
  // HÀM HIỂN THỊ DỮ LIỆU USER LÊN MODAL
  function showDataUser(paramUser) {
    "use strict";
    $("#lb-name").html(paramUser.hoVaTen);
    $("#lb-email").html(paramUser.email);
    $("#lb-sodienthoai").html(paramUser.dienThoai);
    $("#lb-diachi").html(paramUser.diaChi);
    $("#lb-loinhan").html(paramUser.loiNhan);

    $("#lb-size").html(paramUser.menuDuocChon.menuName);
    $("#lb-duongkinh").html(paramUser.menuDuocChon.duongKinhCM);
    $("#lb-suon").html(paramUser.menuDuocChon.suonNuong);
    $("#lb-nuocngot").html(paramUser.menuDuocChon.drink);
    $("#lb-loaipizza").html(paramUser.loaiPizza);
    $("#lb-loainuocuong").html(paramUser.loaiNuocUong);
    $("#lb-gia").html(paramUser.menuDuocChon.priceVND);
    $("#lb-mavourcher").html(paramUser.voucher);
    getVoucherDiscount()


    $("#lb-phantramgiamgia").html(gDiscount.phanTramGiamGia + "%");

    var vThanhToan = paramUser.menuDuocChon.priceVND * (1 - gDiscount.phanTramGiamGia / 100);
    $("#lb-thanhtoan").html(vThanhToan + " Đồng");
  };
  // Hàm xử lý voucher
  function getVoucherDiscount() {
    "use strict";
    var vVoucherId = $("#inp-voucher").val();
    $.ajax({
      async: false,
      type: "GET",
      url: "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + vVoucherId,
      dataType: "json",
      success: function (vVourcherObject) {
        gDiscount = { phanTramGiamGia: vVourcherObject.phanTramGiamGia };
        console.log(gDiscount)
      },
    });
  };

  // HÀM GỌI SEVER ĐỂ TẠO ĐƠN HÀNG MỚI
  function createOrder() {
    "use strict";
    //base url
    var vInputSend = JSON.stringify(gUser);
    $.ajax({
      type: "POST",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      data: vInputSend,
      contentType: "application/json",
      success: function (vOrderDetailObject) {
        console.log(vOrderDetailObject);
        displayOrderCode(vOrderDetailObject);
        console.log(vOrderDetailObject);
        gId = vOrderDetailObject.orderId;
      },
    });
  };
  function displayOrderCode(paramOrderObject) {
   $("#result").modal("show");
  
    var vOrderCode = $("#inp-madonhang");
    vOrderCode.val(paramOrderObject.orderId);
    resetForm();
  }
  function resetForm() {
    "use strict";
    $("#inp-fullname").val("");
    $("#inp-email").val("");
    $("#inp-dien-thoai").val("");
    $("#inp-dia-chi").val("");
    $("#inp-message").val("");
    $("#inp-voucher").val("");
    $("#select-drink").val("");

    $("#btn-combo-small").addClass("btn-warning");
    $("#btn-combo-medium").addClass(" btn-warning");
    $("#btn-combo-large").addClass("btn-warning");

    $("#btn-pizza-hawai").addClass("btn-warning");
    $("#btn-pizza-seafood").addClass(" btn-warning");
    $("#btn-pizza-bacon").addClass("btn-warning")
  };
 
//////////////////////////////////////////////////////
//Task55.10
/////////////////////////////
function showMessage(paramDay) {
  var note = "";
  var messageArr = [
    { date: "Monday", message: "Thu 2 mua 02 tang 1" },
    { date: "Tuesday", message: "Thu 3 mua 02 tang 3" },
    { date: "Wednesday", message: "Thu 4 mua 02 tang 4" },
    { date: "Thursday", message: "Thu 5 mua 02 tang 5" },
    { date: "Friday", message: "Thu 6 mua 02 tang 6" },
    { date: "Saturady", message: "Thu 7 mua 02 tang 7" },
    { date: "Sunday", message: "Thu 8 mua 02 tang 8" },

    { date: "Thứ Hai", message: "Thu 2 mua 02 tang 1" },
    { date: "Thứ Ba", message: "Thu 3 mua 02 tang 3" },
    { date: "Thứ Tư", message: "Thu 4 mua 02 tang 4" },
    { date: "Thứ Năm", message: "Thu 5 mua 02 tang 5" },
    { date: "Thứ Sáu", message: "Thu 6 mua 02 tang 6" },
    { date: "Thứ Bảy", message: "Thu 7 mua 02 tang 7" },
    { date: "Chủ nhật", message: "Thu 8 mua 02 tang 8" },
  ];
  for (var bI = 0; bI < messageArr.length; bI++) {
    if (paramDay == messageArr[bI].date) {
      note = messageArr[bI].message;
      console.log(note);
    }
  }
  $("#message").html(note);
}

});